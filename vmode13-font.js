(function () {
  class Mode13Font {
    constructor(draw, font, w, h, swap = false) {
      this.draw = draw;
      this.font = font;
      this.w = w | 0;
      this.h = h | 0;
      this.swap = swap;
    }

    drawChar(color, x, y, i) {
      x = x | 0;
      y = y | 0;
      i = i | 0;
      for (var cy = 0 | 0; cy < this.h; cy++) {
        var byteIndex = i * this.h + cy | 0;
        for (var cx = 0 | 0; cx < this.w; cx++) {
          if (this.swap) {
            var bitValue = (this.font[byteIndex] >> (7 - cx)) & 1;
          } else {
            var bitValue = (this.font[byteIndex] >> cx) & 1;
          }
          bitValue ? this.draw.setPixel(x + cx, y + cy, color) : 0;
        }
      }
    }

    drawString(color, x, y, text) {
      x = x | 0;
      y = y | 0;
      for (var i = 0 | 0; i < text.length; i++) {
        this.drawChar(color, x + i * this.w, y, text.charCodeAt(i));
      }
    }
  }

  window.Mode13Font = Mode13Font;
})();