import globals from "globals";
import pluginJs from "@eslint/js";


export default [
  {files: ["**/*.js"], languageOptions: {sourceType: "script"}},
  {
    languageOptions: {
      globals: globals.browser,
      parserOptions: {
        ecmaVersion: 3,
        sourceType: "script"
      },      
    },
  },
  pluginJs.configs.recommended,
];

// {
//   "env": {
//     "browser": true,
//     "node": true
//   },
//   "rules": {
//     "no-var": "error",
//     "prefer-const": "error",
//     "prefer-arrow-callback": "error",
//     "no-template-curly-in-string": "error"
//   }
// }
