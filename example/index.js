/*
This example is fully released into the public domain, I may someday choose to create a vmode13-ui
that uses some of these primitives, but for now they are yours to consume however you like. This
most definitely has bugs but was good enough for this demo.
*/

(function () {
  async function loadDFF(fonturl, draw) {
    return await fetch(fonturl)
      .then(response => response.arrayBuffer())
      .then(arraybuf => new Uint8Array(arraybuf,1))
  }

  class Rect {
    constructor(pos, size) {
      this.pos = pos;
      this.size = size;
    }

    test(pos) {
      var x = pos.x - this.pos.x;
      var y = pos.y - this.pos.y;

      return (x > 0 && y > 0 && x < this.size.w && y < this.size.h)
    }
  }

  class Panel {
    constructor(mode13, draw, pos, size, color=7) {
      this.mode13 = mode13;
      this.pos = pos;
      this.size = size;      
      this.draw = draw;
      this.color = color;
    }

    render() {
      this.draw.panel(this.pos.x, this.pos.y, this.pos.x+this.size.w, this.pos.y+this.size.h,  15, 8, this.color);
    }
  }

  class DragPanel extends Panel {
    constructor(mode13, draw, pos, size, renderer) {
      super(mode13, draw, pos, size);
      this.renderer = renderer;

      this.mb = new Rect(pos, size);
      this.dragging = false;      
      this.mouseIn;
      this.lastPos = {x:0, y:0};
    }

    // since we eschew the event model, in favor of detecting events (stateful / retro style)
    // we need to 'edge detect' the start of a dragging operation
    update() {
      const mouseIn = this.mouseIn = this.mb.test(this.mode13.mouse.pos);
      if (!this.dragging) {
        this.dragging = this.mode13.mouse.btnLeft && mouseIn;
        const mx = this.mode13.mouse.pos.x;
        const my = this.mode13.mouse.pos.y;                
        this.lastPos = {x: mx, y: my};
      } else {
        if (this.mode13.mouse.btnLeft) {
          const mx = this.mode13.mouse.pos.x;
          const my = this.mode13.mouse.pos.y;
          
          this.pos.x += (mx - this.lastPos.x);
          this.pos.y += (my - this.lastPos.y);

          this.lastPos = {x: mx, y: my};
        } else {
          this.dragging = false;
        }
      }      
    }

    render() {
      super.render();
      if (this.renderer) {
        this.renderer.apply(this, [this]);
      }
    }
  }

  class Button extends Panel {
    constructor(mode13, draw, pos, font, label, activate, style) {
      super(mode13, draw, pos, {w:0, h:0});

      this.font = font;

      style = style || {};
      style.vpadding = style.vpadding || 2;
      style.hpadding = style.hpadding || 2;

      if (style && style.padding) {
        style.vpadding = style.padding;
        style.hpadding = style.padding;
      }
      
      this.style = style;
      if (style && style.vpadding) {
        this.style.topPad = style.vpadding;
        this.style.botPad = style.vpadding;
      }
      if (style && style.hpadding) {
        this.style.leftPad = style.hpadding;
        this.style.rightPad = style.hpadding;
      }
      delete this.style.padding;
      delete this.style.vpadding;
      delete this.style.hpadding;

      this.style.topPad = this.style.topPad || 2;
      this.style.botPad = this.style.botPad || 2;
      this.style.leftPad = this.style.leftPad || 2;
      this.style.rightPad = this.style.rightPad || 2;

      this.style.color = this.style.color || 7;
      this.style.brightColor = this.style.brightColor || 15;
      this.style.darkColor = this.style.darkColor || 8;

      this.size = {w:label.length * font.w + this.style.leftPad + this.style.rightPad, h: font.h + this.style.topPad + this.style.botPad};

      this.isActive = false;
      this.label = label;
      this.activate = activate;
      this.activating = false;
      this.mb = new Rect(pos, this.size);
    }

    update() {
      const mouseIn = this.mb.test(this.mode13.mouse.pos);
      if (!this.activating) {
        if (mouseIn && this.mode13.mouse.btnLeft) {
          this.activating = true;
        }
      } else {
        if (!this.mode13.mouse.btnLeft) {
          this.activating = false;
          if (mouseIn) {
            this.activate && this.activate();
          }
        }
      }
      
      return this.activating;
    }

    render() {
      if (this.activating && this.mb.test(this.mode13.mouse.pos)) {
        this.draw.panel(this.pos.x, this.pos.y, this.pos.x+this.size.w, this.pos.y+this.size.h, this.style.darkColor, this.style.brightColor, this.style.color);
      } else {
        this.draw.panel(this.pos.x, this.pos.y, this.pos.x+this.size.w, this.pos.y+this.size.h, this.style.brightColor, this.style.darkColor, this.style.color);
      }
      
      this.font.drawString(4, this.pos.x + this.style.leftPad, this.pos.y + this.style.topPad, this.label);
    }
  }

  async function main() {
    const mode13 = new Mode13({ el: document.getElementById("Mode13Container"), width:320, height:200});
    const draw = new Mode13Draw(mode13);

    mode13.hideModernCursor();
    mode13.showRetroCursor();

    const dnd = new Mode13Font(draw, await loadDFF("AD&D.DFF", draw), 8, 16, true);
    const font = new Mode13Font(draw, await loadDFF("font", draw), 8, 14);

    const statusPanel = new DragPanel(mode13, draw, {x: 30, y: 80}, {w: 200, h: 2+(14*3)+2}, function() {
      font.drawString(4, this.pos.x + 5, this.pos.y + 2, `Mouse: ${this.mode13.mouse.pos.x}, ${this.mode13.mouse.pos.y}`);
      font.drawString(4, this.pos.x + 5, this.pos.y + 2 + 14, `${this.mode13.mouse.btnLeft}, ${this.mode13.mouse.btnRight}, ${this.mode13.mouse.btnMid}`);
      font.drawString(4, this.pos.x + 5, this.pos.y + 2 + 28, `${this.mouseIn}`);
    });

    let drawBg = true;

    const button = new Button(mode13, draw, {x: 10, y: 10}, dnd, "Toggle BG", () => {
      drawBg = !drawBg;
    });
    
    const points = [
      { pos: { x: Math.random() * mode13.width, y: Math.random() * mode13.height }, vel: { x: 2, y: 2 } },
      { pos: { x: Math.random() * mode13.width, y: Math.random() * mode13.height }, vel: { x: 2, y: 2 } },
      { pos: { x: Math.random() * mode13.width, y: Math.random() * mode13.height }, vel: { x: 2, y: 2 } }
    ];

    let skip = false;
    mode13.renderLoop((t) => {
      if (skip) {
        skip = false;
        return true;
      }
      skip = true;

      draw.clear();

      dnd.drawChar(0xF, 10, 10, 'A'.charCodeAt(0));
      dnd.drawString(4, Math.floor(t*3), 30, "This should be red!");

      points.forEach(point => {
        point.pos.x += point.vel.x;
        point.pos.y += point.vel.y;
    
        if (point.pos.x <= 0 || point.pos.x >= mode13.width-1) {
          point.vel.x *= -1;
        }
        if (point.pos.y <= 0 || point.pos.y >= mode13.height-1) {
          point.vel.y *= -1;
        }
      });

      drawBg && draw.rect(0, 0, 320, 200, 3);

      draw.line(points[0].pos.x, points[0].pos.y, points[1].pos.x, points[1].pos.y, 10);
      draw.line(points[1].pos.x, points[1].pos.y, points[2].pos.x, points[2].pos.y, 10);
      draw.line(points[2].pos.x, points[2].pos.y, points[0].pos.x, points[0].pos.y, 10);
      
      draw.circle(100.5, 100, 50, 4);      

      statusPanel.update();
      button.update();
      button.render();
      statusPanel.render();       
      return true;
    });
  }

  main();
})();
