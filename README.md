# vMode13 - Virtual Mode 13

Welcome to vMode13, this is my "fantasy computer target" for javascript.  The eventual goal is to be a fantasy "PC" ala retro ibm pc's from the late 80's early 90's (think, Commander Keen/Doom era)

I'm not 100% ready to release my fantasy computer spec, but I've noodled with this library for so long that it's just time to push it.

vMode13 uses webgl to create a graphics display using any arbitray resolution,  I use a shader to transmit the pixel data, and there is an 8 bit palette 

refer to the example folder for a working example with the exras that I implemented, including DOS font rendering (old bitmap fonts) and some primitive drawing routines, plus a rudimentary framework for the ui

eventually I'm going to create a full 'vMode13' spec that can be targeted as a virtual retro PC.  But this is a start

## example
```html
<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Virtual Mode 13</title>
    <style>
    html, body {
        font-family: arial;
        margin: 0;
        padding: 0;
        width: 100%;
        height: 100%;
        background-color: #333344;
    }
    </style>
    <link rel="stylesheet" href="/vmode13.css" />
</head>

<body>
    <div id='Mode13Container'></div>
    <script src="/vmode13.js"></script>
    <script>
    const mode13 = new Mode13({ el: document.getElementById("Mode13Container"), width:320, height:200});
    mode13.renderLoop((t) => {
        // read and write to the mode13.buffer to set pixel data, the buffer is width*height bytes in size
        mode13.buffer[0] = 6; // pixel in top left corner set
    });
    </script>
</body>
</html>
```

## API's

The packages has these apis available

- [x] Video display with palette
- [x] mouse, with mouse cursor
- [x] Drawing primitives
- [ ] Keyboard
- [ ] Joystick

## Future

I have a reason for replacing all the apis, there will be an ecmascript C/C++ tooling available, and I plan on making an actual DOS compatible lib that will allow retro builds for retro machines! (pipe dream, distant future!) and maybe SDL etc...
