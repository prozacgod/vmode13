(function () {
  class Mode13Draw {
    constructor(mode13) {
      this.buffer = mode13.buffer;
      this.width = mode13.width;
      this.height = mode13.height;
    }

    circle(x0, y0, radius, color) {
      x0 = x0 | 0;
      y0 = y0 | 0;
      radius = radius | 0;
      color = color | 0;
      let x = radius | 0;
      let y = 0 | 0;
      let err = 0 | 0;

      while (x >= y) {
        this.setPixel(x0 + x, y0 + y, color);
        this.setPixel(x0 + y, y0 + x, color);
        this.setPixel(x0 - y, y0 + x, color);
        this.setPixel(x0 - x, y0 + y, color);
        this.setPixel(x0 - x, y0 - y, color);
        this.setPixel(x0 - y, y0 - x, color);
        this.setPixel(x0 + y, y0 - x, color);
        this.setPixel(x0 + x, y0 - y, color);

        if (err <= 0) {
          y += 1;
          err += 2 * y + 1;
        }

        if (err > 0) {
          x -= 1;
          err -= 2 * x + 1;
        }
      }
    }

    line(x0, y0, x1, y1, c) {
      x0 = x0 | 0;
      y0 = y0 | 0;
      x1 = x1 | 0;
      y1 = y1 | 0;
      c = c | 0;

      var dx = Math.abs(x1 - x0) | 0;
      var dy = Math.abs(y1 - y0) | 0;
      var sx = (x0 < x1 ? 1 : -1) | 0;
      var sy = (y0 < y1 ? 1 : -1) | 0;
      var err = (dx - dy) | 0;

      while (true) {
        this.setPixel(x0, y0, c);
        if (x0 === x1 && y0 === y1) break;
        var e2 = 2 * err | 0;
        if (e2 > -dy) {
          err -= dy;
          x0 += sx;
        }
        if (e2 < dx) {
          err += dx;
          y0 += sy;
        }
      }
    }

    setPixel(x, y, c) {
      x=x|0;
      y=y|0;
      c=c|0;
      if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
        this.buffer[y * this.width + x] = c;
      }
    }

    hline(x1, x2, y, c) {
      x1=x1|0;
      x2=x2|0;
      y=y|0;
      c=c|0;
      for (var x = x1|0; x < x2; x++) {
        this.setPixel(x, y, c);
      }
    }

    vline(y1, y2, x, c) {
      y1=y1|0;
      y2=y2|0;
      x=x|0;
      c=c|0;
      for (let y = y1; y < y2; y++) {
        this.setPixel(x, y, c);
      }
    }

    rect(x1, y1, x2, y2, c) {
      x1=x1|0;
      y1=y1|0;
      x2=x2|0;
      y2=y2|0;
      c=c|0;
      for (var y = y1|0; y < y2; y++) {
        this.hline(x1, x2, y, c);
      }
    }

    panel(x1, y1, x2, y2, top_edge, bottom_edge, panel_color) {
      this.rect(x1, y1, x2, y2, panel_color);
      this.hline(x1, x2, y1, top_edge);
      this.vline(y1, y2, x1, top_edge);
      this.hline(x1, x2, y2 - 1, bottom_edge);
      this.vline(y1, y2, x2 - 1, bottom_edge);
    }

    clear() {
      this.buffer.fill(0);
    }
  }
  
  window.Mode13Draw = Mode13Draw;
}) ();