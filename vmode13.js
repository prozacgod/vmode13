/* This library is SAD */
(function() {
  var vertex_shader = "precision mediump float;\nattribute vec4 a_position;\nvarying vec2 v_texcoord;\n\nvoid main() {\n  gl_Position = a_position;\n  v_texcoord = a_position.xy * vec2(0.5, -0.5) + 0.5;\n}\n";
  var fragment_shader = "precision mediump float;\nvarying vec2 v_texcoord;\n\nuniform sampler2D u_image;\nuniform sampler2D u_palette;\n\nvoid main() {\n  float index = texture2D(u_image, v_texcoord).a * 255.0;\n  gl_FragColor = texture2D(u_palette, vec2((index + 0.5) / 256.0, 0.5));\n}\n";

  // Setup basic palette.
  var palette16 = new Uint8Array([
    0x00, 0x00, 0x00, 0xFF,
    0x00, 0x00, 0xAA, 0xFF,
    0x00, 0xAA, 0x00, 0xFF,
    0x00, 0xAA, 0xAA, 0xFF,
    0xAA, 0x00, 0x00, 0xFF,
    0xAA, 0x00, 0xAA, 0xFF,
    0xAA, 0x55, 0x00, 0xFF,
    0xAA, 0xAA, 0xAA, 0xFF,
    0x55, 0x55, 0x55, 0xFF,
    0x55, 0x55, 0xFF, 0xFF,
    0x55, 0xFF, 0x55, 0xFF,
    0x55, 0xFF, 0xFF, 0xFF,
    0xFF, 0x55, 0x55, 0xFF,
    0xFF, 0x55, 0xFF, 0xFF,
    0xFF, 0xFF, 0x55, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF
  ]);

  // Mouse Cursor 0xFF is transparent
  var defaultCursor = (function() {
    var b = 0x00;
    var w = 0x0f;
    var _ = 0xFF;
  
    return new Uint8Array([
      b,b,_,_,_,_,_,_,_,_,_,_,_,_,_,_,
      b,w,b,_,_,_,_,_,_,_,_,_,_,_,_,_,
      b,w,w,b,_,_,_,_,_,_,_,_,_,_,_,_,
      b,w,w,w,b,_,_,_,_,_,_,_,_,_,_,_,
      b,w,w,w,w,b,_,_,_,_,_,_,_,_,_,_,
      b,w,w,w,w,w,b,_,_,_,_,_,_,_,_,_,
      b,w,w,w,w,w,w,b,_,_,_,_,_,_,_,_,
      b,w,w,w,w,w,w,w,b,_,_,_,_,_,_,_,
      b,w,w,w,w,w,w,w,w,b,_,_,_,_,_,_,
      b,w,w,w,w,w,b,b,b,b,b,_,_,_,_,_,
      b,w,w,b,w,w,b,_,_,_,_,_,_,_,_,_,
      b,w,b,b,b,w,w,b,_,_,_,_,_,_,_,_,
      b,b,_,_,b,w,w,b,_,_,_,_,_,_,_,_,
      _,_,_,_,_,b,w,w,b,_,_,_,_,_,_,_,
      _,_,_,_,_,b,w,w,b,_,_,_,_,_,_,_,
      _,_,_,_,_,_,b,b,_,_,_,_,_,_,_,_,
    ]);
  })();

  var triangles = [
    1,  1,
    -1,  1,
    -1, -1,
    1,  1,
    -1, -1,
    1, -1,
  ];

  function createShader (gl, sourceCode, type) {
    var shader = gl.createShader( type );
    gl.shaderSource( shader, sourceCode );
    gl.compileShader( shader );
  
    if ( !gl.getShaderParameter(shader, gl.COMPILE_STATUS) ) {
      var log = gl.getShaderInfoLog( shader );
      gl.deleteShader(shader);
      throw 'Could not compile WebGL program. \n\n' + log;
    }

    return shader;
  }

  function Mode13(opts) {
    var targetElement = this.targetElement = opts.el;
    var canvas = this.canvas = opts.canvas;

    this.width = opts.width || 320;
    this.height = opts.height || 200;

    if (!canvas) {
      canvas = this.canvas = document.createElement('canvas');
      canvas.className = 'Mode13Canvas';
      canvas.width = this.width;
      canvas.height = this.height;
      canvas.oncontextmenu = function(evt) { evt.preventDefault() };
      canvas.tabindex = -1;
      targetElement.appendChild(canvas);
    }

    this.mouse = {
      pos: {x: 0, y: 0},
      retro: false,
      modern: true,
      btnLeft: false,
      btnRight: false,
      btnMid: false,
      cursor: new Uint8Array(Mode13.defaultCursor)
    };
    
    //TODO: um, release handlers?
    this._mouseDownHandler = this.onMouseDown();
    this._mouseUpHandler = this.onMouseUp();
    this._mouseMoveHandler = this.onMouseMove();

    var gl = this.gl = canvas.getContext("webgl");
    
    var vshader = createShader(gl, Mode13.vertex_shader, gl.VERTEX_SHADER);
    var fshader = createShader(gl, Mode13.fragment_shader, gl.FRAGMENT_SHADER);
    
    var program = gl.createProgram();
    gl.attachShader(program, vshader);
    gl.attachShader(program, fshader);

    gl.linkProgram(program);

    var linked = gl.getProgramParameter(program, gl.LINK_STATUS);

    if (!linked) {
      var log = gl.getProgramInfoLog(program);
      gl.deleteProgram(program);
      gl.deleteRenderbuffer(gl, vshader);
      gl.deleteRenderbuffer(gl, fshader);

      throw("Error in linking: " + log);
    }

    var a_position = gl.getAttribLocation(program, 'a_position');

    gl.bindAttribLocation(program, a_position, 'a_position');
    
    gl.useProgram(program);

    var u_image = gl.getUniformLocation(program, 'u_image');
    gl.uniform1i(u_image, 0);
    
    var u_palette = gl.getUniformLocation(program, 'u_palette');
    gl.uniform1i(u_palette, 1);

    //var u_palette = gl.getUniformLocation(program, 'u_cursor');
    //gl.uniform1i(u_palette, 1);

    var vertBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangles), gl.STATIC_DRAW);
    gl.enableVertexAttribArray(a_position);

    gl.vertexAttribPointer(a_position, 2, gl.FLOAT, false, 0, 0);

    var palette = new Uint8Array(256*4);
    palette.set(Mode13.palette16);
    
    // make palette texture and upload palette
    gl.activeTexture(gl.TEXTURE1);
    var paletteTex = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, paletteTex);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 256, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, palette);

    this.buffer = new Uint8Array(this.width*this.height);
    // make image textures and upload image
    gl.activeTexture(gl.TEXTURE0);
    
    var imageTex = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, imageTex);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.ALPHA, this.width, this.height, 0, gl.ALPHA, gl.UNSIGNED_BYTE, this.buffer);
        
    gl.drawArrays(gl.TRIANGLES, 0, triangles.length / 2);
  }

  Mode13.palette16 = palette16;
  Mode13.defaultCursor = defaultCursor;
  Mode13.vertex_shader = vertex_shader;
  Mode13.fragment_shader = fragment_shader;

  // Mode13.prototype.setCanvasCorrection = function(w, h) {
  //   this.canvas.sty    
  // }

  Mode13.prototype.blit_buffer = function(buffer) {
    var gl = this.gl;
    gl.activeTexture(gl.TEXTURE0);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.ALPHA, this.width, this.height, 0, gl.ALPHA, gl.UNSIGNED_BYTE, buffer);
    gl.drawArrays(gl.TRIANGLES, 0, triangles.length / 2);
  }

  Mode13.prototype.set_palette = function(palette) {
    var gl = this.gl;
    gl.activeTexture(gl.TEXTURE1);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 256, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, palette);
    gl.drawArrays(gl.TRIANGLES, 0, triangles.length / 2);
  }

  Mode13.prototype.onMouseMove = function() {
    var self = this;
    function _mouseMoveHandler(evt) {
      var rect = evt.target.getBoundingClientRect();
      var x = (((evt.clientX-rect.left) / rect.width) * self.width)|0;
      var y = (((evt.clientY-rect.top) / rect.height) * self.height)|0;
      
      self.mouse.pos.x = x;
      self.mouse.pos.y = y;
    }

    this.canvas.addEventListener('mousemove', _mouseMoveHandler);
    return function() { self.canvas.removeEventListener(_mouseMoveHandler) };
  }

  Mode13.prototype.onMouseDown = function() {
    var self = this;
    function _mouseDownHandler(evt) {
      if (evt.button == 0) {
        self.mouse.btnLeft = true;
      } else if (evt.button == 2) {
        self.mouse.btnRight = true;
      } else if (evt.button == 1) {
        self.mouse.btnMid = true;
      }
    }

    this.canvas.addEventListener('mousedown', _mouseDownHandler);
    return function() { self.canvas.removeEventListener(_mouseDownHandler) };
  }

  Mode13.prototype.onMouseUp = function() {
    var self = this;
    function _mouseUpHandler(evt) {
      if (evt.button == 0) {
        self.mouse.btnLeft = false;
      } else if (evt.button == 2) {
        self.mouse.btnRight = false;
      } else if (evt.button == 1) {
        self.mouse.btnMid = false;
      }
    }

    this.canvas.addEventListener('mouseup', _mouseUpHandler);
    return function() { self.canvas.removeEventListener(_mouseUpHandler); };
  }

  Mode13.prototype.hideModernCursor = function() {
    this.canvas.className = 'Mode13Canvas HideModernCursor';
    this.mouse.modern = false;
  }

  Mode13.prototype.showModernCursor = function() {
    this.canvas.className = 'Mode13Canvas';
    this.mouse.modern = true;
  }

  Mode13.prototype.showRetroCursor = function() {
    this.mouse.retro = true;
  }

  Mode13.prototype.hideRetroCursor = function() {
    this.mouse.retro = false;
  }

  Mode13.prototype.renderLoop = function(renderCb) {
    var self = this;
    var start = 0;
    var _frame = function (ts) {
      if (start === undefined) {
        start = ts;
      }

      var elapsed = (ts - start) / 1000;
      
      if (renderCb(elapsed)) {
        if (self.mouse.retro) {
          var index = self.mouse.pos.y * self.width + self.mouse.pos.x;
          for (var y = 0; y < 16; y++) {
            for (var x = 0; x < 16; x++) {
              var xx = self.mouse.pos.x + x;
              var yy = self.mouse.pos.y + x;
              var c = self.mouse.cursor[y*16+x];
              if (c !== 0xFF) {
                if ((xx < self.width) && (yy < self.height))
                  self.buffer[index+x] = c;
              }
            }              
            index += self.width;
          }
        }
        self.blit_buffer(self.buffer);
        self.animationHandle = requestAnimationFrame(_frame);
      }
    }

    self.animationHandle = requestAnimationFrame(_frame);
  }
  
  Mode13.prototype.destroy = function() {
    if (self.animationHandle) {
      cancelAnimationFrame(self.animationHandle);
      delete self.animationHandle;
    }

    this._mouseDownHandler();
    this._mouseUpHandler();
    this._mouseMoveHandler();
  }

  window.Mode13 = Mode13;
})();